# 参考: https://docs.docker.com/compose/install/
# 直接跑如下命令即可：
git clone https://gitee.com/shihowcom/setup_docker_compose.git && \
    cp ./setup_docker_compose/docker-compose-linux-aarch64 /usr/local/bin/docker-compose && \
    chmod +x /usr/local/bin/docker-compose && \
    ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
